package carrentcustomerfacing.com.customerapp;

public class UserDetails
{
    public String fullname;
    public String phonenumber;
    public String email;


    public String idnumber;
    public UserDetails()
    {

    }
    public UserDetails(String fullname,String phonenumber,String email,String idnumber)
    {
        this.fullname=fullname;
        this.phonenumber=phonenumber;
        this.email=email;
        this.idnumber=idnumber;
    }
    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }


}
