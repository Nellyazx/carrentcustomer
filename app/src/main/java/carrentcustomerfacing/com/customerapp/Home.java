package carrentcustomerfacing.com.customerapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.shaishavgandhi.loginbuttons.GoogleButton;

public class Home extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener
{
    EditText fullname,email,phonenumber,Id;
    TextView complete;
    ProgressDialog progressDialog;
    ImageView profilePhoto;
    String fname,mail,phone,nationalid;
    FirebaseAuth firebaseAuth;
    GoogleButton googleButton;
    private static final int RC_SIGN_IN=100;
    GoogleApiClient googleApiClient;
    String myname,myemail,myurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        firebaseAuth = FirebaseAuth.getInstance();
        fullname = findViewById(R.id.editText);
        email = findViewById(R.id.editText2);
        phonenumber = findViewById(R.id.editText3);
        Id = findViewById(R.id.editText4);
        complete = findViewById(R.id.textView3);
        progressDialog = new ProgressDialog(this);
        googleButton = findViewById(R.id.googleButton);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleApiClient= new GoogleApiClient.Builder(this).enableAutoManage(this,  this).addApi(Auth.GOOGLE_SIGN_IN_API,gso).build();

        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent,RC_SIGN_IN);
            }
        });
        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RC_SIGN_IN)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }

    public void validateFields()
    {
        fname=fullname.getText().toString().trim();
        mail=email.getText().toString().trim();
        phone=phonenumber.getText().toString().trim();
        nationalid=Id.getText().toString().trim();
        if(TextUtils.isEmpty(fname))
        {
            Toast.makeText(this, "Please Enter First Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(mail))
        {
            Toast.makeText(this, "Please Enter Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(phone))
        {
            Toast.makeText(this, "Please Enter Phone Number", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(nationalid))
        {
            Toast.makeText(this, "Please Enter National Id", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Completing User Profile...");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(mail,nationalid)
        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressDialog.dismiss();
                if(task.isSuccessful())
                {
                    UserDetails user = new UserDetails(fname,phone,mail,nationalid);
                    FirebaseDatabase.getInstance().getReference("Users")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            progressDialog.dismiss();

                            if(task.isSuccessful())
                            {
                                Toast.makeText(Home.this, "Profile Update Successfull", Toast.LENGTH_SHORT).show();
                                Intent myData = new Intent(Home.this,Main2Activity.class);
                                startActivity(myData);
                            }
                            else
                            {
                                progressDialog.dismiss();

                                Toast.makeText(Home.this, "Error Completing profile", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(Home.this, ""+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

//        if(firebaseAuth.getCurrentUser().getEmail()!=null)
//        {
//            finish();
//            startActivity(new Intent(Home.this,Main2Activity.class));
//        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public void handleResult(GoogleSignInResult result)
    {
        if(result.isSuccess())
        {
            //get username,emaila nad profile pic
            GoogleSignInAccount accunt = result.getSignInAccount();
             myname = accunt.getDisplayName();
             myemail = accunt.getEmail();
             myurl = accunt.getPhotoUrl().toString();
             Intent myData = new Intent(Home.this,Main2Activity.class);
             myData.putExtra("name",myname);
             myData.putExtra("email",myemail);
             myData.putExtra("url",myurl);
             startActivity(myData);
             //Check for next Screen
        }
    }
}
