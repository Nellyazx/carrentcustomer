package carrentcustomerfacing.com.customerapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {
    Button content;
    String name,email,url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        content = findViewById(R.id.button2);
        Intent myData = getIntent();
        name=myData.getStringExtra("name");
        email=myData.getStringExtra("email");
        url=myData.getStringExtra("url");
        content.setText("Name "+name+" Email "+email+" Url"+url);
    }
}
